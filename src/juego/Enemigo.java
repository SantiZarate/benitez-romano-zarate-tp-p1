package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {
	
	private double x;
	private double y;
	private double angulo;
	private double velocidad;
	private Image imagen;
	
	public Enemigo(double x, double y, int velocidad, String nombreImagen) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.imagen = Herramientas.cargarImagen(nombreImagen);
	}
		
	public void dibujar(Entorno e) {
		e.dibujarImagen(imagen, x, y, 0, 0.6);
	}
	
	public void avanzarK() {
		this.x += velocidad*Math.cos(angulo);
		this.y += velocidad*Math.sin(angulo);
	}
	
	public void avanzar(int i) {
		y += velocidad   ;
		if(i >= 0 && i <= 30) {
			x -= velocidad + 1 ;
		}else if(i >= 30 && i <= 60) {
			x += velocidad + 1;
		}
	}
	
	public void perseguir (double xnave, double ynave, Entorno e) {
		if (this.y < e.alto()*0.77) {
			this.angulo = Math.atan2(ynave - y , xnave - x);
		}
	}

	public boolean estaFuera(Entorno e) {
		if(this.x < 0 || this.x > e.ancho() + 200 || this.y < -200 || this.y > e.alto() + 200) {
			return false;
		}
		return true;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
