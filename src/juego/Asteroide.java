package juego;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
public class Asteroide {
	
	public double x, y, angulo;
	private Image imagen;
	private double anguloImg;
	private int velocidad;
	
	public Asteroide(double angulo, Entorno e, String nombreImagen) {
		this.x = Math.random() * e.ancho();
		this.y = - 50 - Math.random()* e.alto();
		this.angulo = angulo;
		this.velocidad= 2;
		this.anguloImg = Math.random()*360;
		imagen = Herramientas.cargarImagen(nombreImagen);
	}
	
//	MEOTODOS DE CLASE
	public void dibujarAsteroide(Entorno e) {
		if (imagen != null) {
		e.dibujarImagen(imagen, x, y, anguloImg);
		}
	}
	
	public void avanzar() {
		this.x += velocidad * Math.cos(angulo);
		this.y += velocidad * Math.sin(angulo);
	}
	
	public boolean estaFuera(Entorno e) {
		if(this.x < 0 || this.x >  e.ancho()) {
			return true;
		}
		return false;
	}
	
	public void girar() {
		this.anguloImg = this.anguloImg - 0.01;
	}

	public void cambiarAngulo() {
		this.angulo += Math.PI/2;
		
	}
	
//	GETTERS
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
}
