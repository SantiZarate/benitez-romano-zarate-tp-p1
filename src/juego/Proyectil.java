package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	private double x;
	private double y;
	private Image imagen;
	
	public Proyectil(double x, double y, String nombreImagen) {
		this.x = x;
		this.y = y;
		this.imagen = Herramientas.cargarImagen(nombreImagen);
	}
	
	public void dibujar(Entorno e) {
		if(imagen != null) {
			e.dibujarImagen(imagen, x, y, 0);
		}
	}
	
	public void avanzar(int i) {
		y += i;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}