package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;
import escenarios.Inicio;
import escenarios.Menu;
import escenarios.Nivel;

public class Juego extends InterfaceJuego {

	private Entorno entorno;

//	Escenarios

	private Nivel nivel;
	private Inicio inicio;
	private Menu menu;

//	Auxiliares/ Extras

	private int contadorTicks;
	private Image viñeta;
	private Image icon;

	public Juego() {
		this.entorno = new Entorno(this, "Prueba del Entorno", 800, 600);
		this.nivel = new Nivel(entorno);
		this.inicio = new Inicio();
		this.menu = new Menu();
		this.contadorTicks = 1;
		this.viñeta = Herramientas.cargarImagen("viñeta.png");
		this.icon = Herramientas.cargarImagen("Nave.png");
		this.entorno.setIconImage(icon);
		this.entorno.iniciar();
	}

	public void tick() {
		//PRIMERO SE DEBERIA EJECUTAR EL INICIO CON EL LOGO DE LA UNGS,
		//DESPUES EL MENU CON EL LOGO DEL JUEGO.
		//UNA VEZ EL MENU PASA A SER NULL SE EJECUTA EL JUEGO.

		if (inicio != null) { // EJECUCION DEL INICIO--------------------------------------------
			// SE DIBUJA EL LOGO DE LA UNGS
			inicio.dibujarFondo(entorno);

			// PASADOS 3 SEGUNDOS SE DIBUJA LA ANIMACION
			inicio.dibujarAnimacion(entorno, contadorTicks);

			// Fade in de entrada
			inicio.transicionEntrada(entorno);

			// PARA SALTAR EL INICIO PRESIONAR ENTER
			if (entorno.sePresiono(entorno.TECLA_ENTER) || contadorTicks % 300 == 0) {
				this.inicio = null;
			}

		} else { // EMPIEZA LA EJECUCION DEL MENU
					// ---------------------------------------------------------------
			if (this.menu != null) {

				// DIBUJADO
				menu.dibujar(entorno);

				// TRANCISION ENTRADA
				menu.entrada(entorno);

				// TRANSICION SALIDA
				menu.salida(entorno);

				// SE ESPERA A QUE LA TRANSICION SEA TOTALMENTE NEGRA PARA ELIMINAR EL MENU
				if (menu.getTransicionSalida().esOpaco()) {
					menu = null;
				}
			} else { // EJECUCION JUEGO
						// -------------------------------------------------------------------------
				
				if (entorno.sePresiono('p')) {
					nivel.pausar();
				}

				if (nivel.estaPausado()) {
					nivel.dibujadoJuegoPausado(entorno);

				} else if (nivel.juegoTerminado()) {

					nivel.animExplosiones(entorno, contadorTicks);
					
					nivel.animGameOver(entorno, contadorTicks);

					// SI SE PRESIONA LA TECLA ENTER SE REINICIA EL NIVEL
					if (entorno.sePresiono(entorno.TECLA_ENTER)) 
						this.nivel = new Nivel(entorno);
					
				} else {
					// MIENTRAS EL JUEGO NO ESTE EN PAUSA,
					// SE PUEDEN EJECUTAR LAS LINEAS DE CODIGO SIGUIENTES.
					
					// DIBUJADO DE ELEMENTOS
					// ------------------------------------------------------------------------------

					// fondo ciclado
					nivel.dibujarFondo(entorno);

					// Dibuja los misiles ya lanzados y verifica si choco con el borde
					nivel.dibujarMisiles(entorno);
					
					if(!nivel.juegoCompletado()) {
						//Dibuja Bonificadores
						nivel.dibujarbonificadores(entorno, contadorTicks);
						
						// Movimiento de la nave
						nivel.dibujarNave(entorno, contadorTicks);
						
						// Dibuja iones enemigos
						nivel.dibujarIones(entorno);
						
						// Dibujar enemigos
						nivel.dibujarEnemigos(entorno, contadorTicks);
						
						// dibujado asteroides
						nivel.dibujarAsteroides(entorno);
						
						nivel.dibujarKamikaze(entorno);
					}
					
					nivel.controlPuntaje();
					
					// ANIMACIONES
					// ------------------------------------------------------------------------------

					// ANIMACION AL RECIBIR DAÑO
					nivel.animPantallazos(entorno);

					// ANIMACIONES DE LA EXPLOSION AL COLISIONAR
					nivel.animExplosiones(entorno, contadorTicks);
					
					nivel.animRecargar(entorno);
					
					nivel.animJuegoCompletado(entorno, contadorTicks);
					
					// ANIMACIONES AL INICIAR EL NIVEL
					nivel.animNuevoNivel(entorno, contadorTicks);
					
					// COLISIONES
					// ------------------------------------------------------------------------------

					// COLISION ENTRE LOS ASTEROIDES Y NAVE
					nivel.colisionAsteroidesConNave(entorno);

					// COLISION ENTRE LOS ENEMIGOS Y LA NAVE
					nivel.colisionEnemigosConNave(entorno);
					
					// COLISION ENTRE KAMIKAZE Y NAVE 
					nivel.colisionKamikazeConNave(entorno);

					// COLISION MISIL DE LA NAVE CON LOS ENEMIGOS
					nivel.colisionMisilConEnemigo();

					// COLISION DEL MISIL CON LOS ASTEROIDES
					nivel.colisionMisilConAsteroides();
					
					//Colision ion con nave
					nivel.colisionIonConNave();
					
					// COLISION DEL MISIL CON KAMIKAZE
					nivel.colisionMisilConKamikaze();

					//COLISION DE BONIFICADOR CON NAVE
					nivel.colisionBonificadoresConNave();
					
					// EXTRAS
					// ------------------------------------------------------------------------------

					// DIBUJADO ELEMENTOS HUD
					nivel.dibujarHud(entorno, contadorTicks);
					
					// "Cronometro"
					nivel.aumentarCronometro(entorno);

					// DIBUJADO DE LA TRANSICION DE ENTRADA AL JUEGO
					nivel.entrada(entorno);

					// CONTADOR DE SEGUNDOS
					nivel.aumentarSegundosJuego(contadorTicks);
				}
			}
			//DIBUJADO DE LA VIÑETA
			entorno.dibujarImagen(viñeta, entorno.ancho() / 2, entorno.alto() / 2, 0);
		}
		contadorTicks++;
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}