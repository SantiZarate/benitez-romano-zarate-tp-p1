package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Nave {
	private double x;
	private double y;
	private double velocidad;
	private Image naveImg;
	private boolean recarga;
	
	public Nave(double x, double y, String nombreImagen) {
		this.x = x;
		this.y = y;
		this.velocidad = 5;
		this.naveImg = Herramientas.cargarImagen(nombreImagen);
		this.recarga = false;
//		EXTRAS
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(naveImg, x, y, 0, 0.4);
	}

	public void moverIzq() {
		x -=  velocidad;
	}
	public void moverDer() {
		x += velocidad;
	}

	public void recargar() {
		this.recarga = true;
	}
	
	public void descargar() {
		this.recarga = false;
	}
	
	//GETTERS -------------------------------------------------------
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public boolean getRecarga() {
		return recarga;
	}
}
