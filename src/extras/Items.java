package extras;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Items {	
	private double x;
	private double y;
	private Image bonificador;
	
	public Items(double x, double y, String nameImage) { // nameImage = "vida.png" - "puntoss.png"
		this.x = x;
		this.y = y;
		bonificador = Herramientas.cargarImagen(nameImage);
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(bonificador, this.x, this.y, 0, 0.07);
	}
	public void avanzar() {
		y += 2;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
}
