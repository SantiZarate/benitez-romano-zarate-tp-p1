package extras;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import entorno.Entorno;

public class Animacion {
	private BufferedImage[] allImages;
	private int numeroFrame;
	private double x, y, tamaño;
	private boolean empezarAnimacion;
	
	//SI ES UNA ANIMACION DE NIVELES VA A CAMBIAR DE CARPETAS
	private int numeroCarpetaNivel;
	
	public Animacion(String direccion, double tamaño) {
		this.numeroFrame = 0;
		this.empezarAnimacion = false;
		this.tamaño = tamaño;
		
		//IMPORTANDO IMAGENES
		File path = new File(direccion);
		File[] allfiles = path.listFiles();
		allImages = new BufferedImage[allfiles.length];

		for (int i = 0; i < allfiles.length; i++) {
			try {
				allImages[i] = ImageIO.read(allfiles[i]);
			} catch (IOException e) {
			}
		}
	}
	
	public Animacion() {
		this.numeroCarpetaNivel = 1;
		this.numeroFrame = 0;
		this.empezarAnimacion = false;
		
		String nombreCarp = "res/niveles/" + Integer.toString(numeroCarpetaNivel);
		
		File carpeta = new File(nombreCarp);
		File[] todosLosArchivos = carpeta.listFiles();
		allImages = new BufferedImage[todosLosArchivos.length];

		for (int i = 0; i < todosLosArchivos.length; i++) {
			try {
				allImages[i] = ImageIO.read(todosLosArchivos[i]);
			} catch (IOException e) {
			}
		}
	}

//	METODOS DE CLASE

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(allImages[numeroFrame], this.x, this.y, 0, tamaño);
	}
	
	public void dibujarEnElMedioPantalla(Entorno entorno) {
		entorno.dibujarImagen(allImages[numeroFrame], entorno.ancho()/2, entorno.alto()/2, 0);
	}
	
	public void reproducirExplosion(int contadorTicks, Entorno entorno, int posicionAvanzar) {
		if(empezarAnimacion) {
			if (numeroFrame != allImages.length) {
				dibujar(entorno);
				avanzar(posicionAvanzar);
				if (contadorTicks % 5 == 0) {
					numeroFrame++;
				}
			} else {
				empezarAnimacion = false;
				numeroFrame= 0;
			}
		}
	}
	
	public void reproducirGameOver(Entorno entorno, int contadorTicks) {
		if (numeroFrame != allImages.length) {
			dibujarEnElMedioPantalla(entorno);
			if (contadorTicks % 5 == 0) {
				numeroFrame++;
			}
		} else {
			numeroFrame = 0;
		}
	}
	
	public void reproducirJuegoCompletado(Entorno entorno, int contadorTicks) {
		if (empezarAnimacion) {
			if (numeroFrame != allImages.length) {
				dibujarEnElMedioPantalla(entorno);
				if (contadorTicks % 5 == 0) {
					numeroFrame++;
				}
			} else {
				numeroFrame = 0;
			}
		}
	}
	
	public void reproducirRecarga(Entorno entorno, int ticksRecarga) {
		if (empezarAnimacion) {
			if (numeroFrame != allImages.length) {
				dibujar(entorno);
				if (ticksRecarga % 7 == 0) {
					numeroFrame++;
				}
			} else {
				empezarAnimacion = false;
				numeroFrame= 0;
			}
		}
	}

	public void reproducirNuevoNivel(Entorno entorno, int contadorTicks) {
		if (numeroFrame != allImages.length) {
			dibujarEnElMedioPantalla(entorno);
			if (contadorTicks % 7 == 0)
				numeroFrame++;
		} else {
			numeroFrame = 0;
		}
	}

	public void aumentarNivel() {
		numeroCarpetaNivel++;
		String nombreCarp = "res/niveles/" + Integer.toString(numeroCarpetaNivel);
		
		File carpeta = new File(nombreCarp);
		File[] todosLosArchivos = carpeta.listFiles();
		allImages = new BufferedImage[todosLosArchivos.length];

		for (int i = 0; i < todosLosArchivos.length; i++) {
			try {
				allImages[i] = ImageIO.read(todosLosArchivos[i]);
			} catch (IOException e) {
			}
		}
	}

	public void avanzar(int posicionAvanzar) {
		y= y + posicionAvanzar;
	}

	public void empezarAnimacion(double posNaveX, double posNaveY) {
		this.empezarAnimacion = true;
		this.x = posNaveX;
		this.y = posNaveY;
	}
	
	public void terminarAnimacion() {
		this.empezarAnimacion = false;
	}
	
	public boolean estadoAnimacion() {
		return empezarAnimacion;
	}
}
