package escenarios;

import java.awt.Image;
import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//
//import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import entorno.Entorno;
import entorno.Herramientas;

public class Inicio {
	private Image fondo;
	private Image logo;
	private Image[] animacion;
	private int numeroFrame;
	private BufferedImage[] allImages;
	private Transicion transicion;

	public Inicio() {
		this.fondo = Herramientas.cargarImagen("fondo_inicio.png");
		//LA IMAGEN DE LOGO ES LA ULTIMA DE LA ANIMACION
		this.logo = Herramientas.cargarImagen("logo_inicio.png");

		//IMPORTANDO LAS IMAGENES PARA LA ANIMACION
		File path = new File("res/animacion_inicio");
		File[] allfiles = path.listFiles();
		allImages = new BufferedImage[allfiles.length];
		
		for(int i = 0; i < allfiles.length; i++) {
			try {
				allImages[i] = ImageIO.read(allfiles[i]);
			}catch(IOException e){
			}
		}
		
		this.transicion = new Transicion(255);
		this.numeroFrame = 0;
	}

//	METODOS DE CLASE

	public void dibujarFondo(Entorno e) {
		e.dibujarImagen(this.fondo, e.ancho() / 2, e.alto() / 2, 0, 0.8);
	}

	public void dibujarLogo(Entorno e) {
		e.dibujarImagen(this.logo, e.ancho() / 2, e.alto() / 2, 0);
	}

	public void dibujarFrame(Entorno e) {
		e.dibujarImagen(this.allImages[numeroFrame], e.ancho() / 2, e.alto() / 2, 0);
	}

	public int lengthFramesPrueba() {
		return this.allImages.length;
	}

//	GETTER Y SETTERS

	public int lengthFrames() {
		return this.animacion.length;
	}

	public void aumentarFrame() {
		this.numeroFrame++;
	}

	public int getNumeroFrame() {
		return this.numeroFrame;
	}
	
//	ANIMACION
	public void dibujarAnimacion(Entorno entorno, int contadorTicks) {
		if (contadorTicks > 150) {
			
			//ANIMACION DE LAS ESTRELLAS. PRUEBA METODO RES
			if (numeroFrame != this.allImages.length) {
				dibujarFrame(entorno);
				if (contadorTicks % 6 == 0) {
					numeroFrame++;
				}
			} else {
				dibujarLogo(entorno);
			}
		}
	}

	public void transicionEntrada(Entorno entorno) {
		transicion.dibujar(entorno);
		
		if (!transicion.esTransparente()) {
			transicion.bajarOpacidad();
		}
	}
}