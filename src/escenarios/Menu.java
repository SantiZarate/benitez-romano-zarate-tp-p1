package escenarios;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Menu {
	private Image fondo;
	private Image logo;
	private Transicion transicion;
	private Transicion transicionSalida;
	private boolean enterMenu;

	public Menu() {
		this.fondo = Herramientas.cargarImagen("fondo_menu.jpg");
		this.logo = Herramientas.cargarImagen("logo_menu.png");
		this.transicion = new Transicion(255);
		this.transicionSalida = new Transicion(0);
		this.enterMenu = false;
	}
	
//	GETTER Y SETTERS
	
	public Transicion getTransicionSalida() {
		return transicionSalida;
	}
	
//	 METODOS DE CLASE
	
	public void dibujarFondo(Entorno e) {
		e.dibujarImagen(fondo, e.ancho()/2, e.alto()/2, 0);
	}
	
//	SE USAN DOS METODOS DISTINTOS PORQUE EL LOGO
//	QUEDARIA MUY GRANDE SI NO SE AJUSTA LA ESCALA
	
	public void dibujarLogo(Entorno e) {
		e.dibujarImagen(logo, e.ancho()/2, e.alto()/2, 0, 0.5);
	}
	
	public void entrada(Entorno entorno) {
		transicion.dibujar(entorno);
		
		if (!transicion.esTransparente()) {
			transicion.bajarOpacidad();
		}
	}
	
	public void salida(Entorno entorno) {
		if(entorno.sePresiono(entorno.TECLA_ENTER)) {
			this.enterMenu = true;
			System.out.println(enterMenu);
		}
		
		if (enterMenu) {
			transicionSalida.dibujar(entorno);
			
			if(!transicionSalida.esOpaco()) {
				transicionSalida.subirOpacidad();
			}
		}
	}
	public void dibujar(Entorno entorno) {
		dibujarFondo(entorno);
		dibujarLogo(entorno);
		
		entorno.cambiarFont("Impact", 20, Color.YELLOW);
		entorno.escribirTexto("Presione ENTER", entorno.ancho() / 3 + 70, entorno.alto() - 150);
	}
}