package escenarios;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import entorno.Entorno;
import entorno.Herramientas;
import juego.Proyectil;

public class Hud {
	private Image hudBase;
	private int anchoBarraVida, altoBarraVida, barraVidaTotal;
	private Color colorBarraVida;
	private int colorRojo;
	private int colorVerde;
	
	private BufferedImage[] imagenesCorazon;
	private Image imagenMisil;
	private Image imagenMisilVacio;
	private int cantCorazones;
	private int cantCorazonesMenos;
	private Image marcoBarraVida;
	
	public Hud() {
		this.hudBase = Herramientas.cargarImagen("hud_base.png");
		this.marcoBarraVida = Herramientas.cargarImagen("hud_barravida.png");
		this.anchoBarraVida = 200;
		this.barraVidaTotal = anchoBarraVida;
		this.altoBarraVida = 10;
		this.colorRojo= 0;
		this.colorVerde= 255;
		this.colorBarraVida = new Color(this.colorRojo, this.colorVerde, 0);
		this.imagenMisil = Herramientas.cargarImagen("misil_nave.png");
		this.imagenMisilVacio = Herramientas.cargarImagen("misil_nave_vacio.png");
		
//		DIBUJADO CORAZONES
		File path = new File("res/corazones");
		File[] allfiles = path.listFiles();
		imagenesCorazon = new BufferedImage[allfiles.length];

		for (int i = 0; i < allfiles.length; i++) {
			try {
				imagenesCorazon[i] = ImageIO.read(allfiles[i]);
			} catch (IOException e) {
			}
		}
		
		this.cantCorazones =3;
		this.cantCorazonesMenos =0;
	}
	
	public void dibujar(Entorno entorno, int contadorTicks, double puntaje, int contadorKills, double puntajeParaPasarNivel, double distanciaMiPuntaje, int numNivel) {
		//DIBUJA LOS BORDES DEL HUD
				dibujarBordes(entorno);

				// dibujado de corazones
				dibujarCorazones(entorno);

				// Dibujado barra de vida
				dibujarBarraVida(entorno);

				// SI LA VIDA ES BAJA LA BARRA EMPIEZA A TITILAR
				if (anchoBarraVida() < 120 && contadorTicks % 20 == 0) {
					dibujarBarraVidaAlerta(entorno);
				}

				dibujarMarcoBarraVida(entorno);

				// BARRA DE ABAJO PARA PASAR DE NIVEL

				if (puntaje != 0) {
					distanciaMiPuntaje = 400.0 / (puntajeParaPasarNivel / puntaje);
				}

				entorno.dibujarRectangulo(200, entorno.alto() - 10, 1, 10, 0, Color.WHITE);
				entorno.dibujarRectangulo(600, entorno.alto() - 10, 1, 10, 0, Color.WHITE);

				if (distanciaMiPuntaje != 0) {
					entorno.dibujarRectangulo(200 + distanciaMiPuntaje, entorno.alto() - 10, 4, 10, 0, Color.GREEN);
				}

				entorno.dibujarRectangulo(entorno.ancho() / 2, entorno.alto() - 10, 400, 1, 0, Color.WHITE);
				
				entorno.cambiarFont("impact", 18, Color.CYAN);
				entorno.escribirTexto("Puntos: " + (int) (puntaje), entorno.ancho() - 100, entorno.alto() - 20);
				entorno.escribirTexto("Nivel: " + numNivel, 10 , 40);
				entorno.escribirTexto("Kills: " + contadorKills, entorno.ancho() - 60  , 40);
	}
	
	public void dibujarBordes(Entorno e) {
		e.dibujarImagen(hudBase, e.ancho()/2, e.alto()/2, 0, 1.05);
	}
	
	public void dibujarBarraVida(Entorno e) {
		e.dibujarRectangulo(0 , e.alto() -15, anchoBarraVida, altoBarraVida, 0, colorBarraVida);
	}
	
	public void dibujarBarraVidaAlerta(Entorno e) {
		e.dibujarRectangulo(0 , e.alto() -15, anchoBarraVida, altoBarraVida, 0, Color.WHITE);
	}
	
	public void dibujarMarcoBarraVida(Entorno e) {
		e.dibujarImagen(marcoBarraVida, e.ancho()/2, e.alto()/2, 0);
	}
	
	public void bajarBarraVida(int cantidadDaño) {
		this.anchoBarraVida -= cantidadDaño*2;
		if (this.colorRojo < 250) {
			this.colorRojo += 125;
		}
		
		if(this.colorVerde > 10) {
			this.colorVerde -= 125;
		}
		this.colorBarraVida = new Color(this.colorRojo, this.colorVerde, 0);
	}
	
	public void restablecerBarraVida() {
		this.anchoBarraVida = this.barraVidaTotal;
		this.colorRojo= 0;
		this.colorVerde= 255;
		this.colorBarraVida = new Color(this.colorRojo, this.colorVerde, 0);
	}

	public void dibujarCorazones(Entorno e) {
		for(int i= 0; i< cantCorazones; i++) {
			if(i >= cantCorazonesMenos) {
				e.dibujarImagen(imagenesCorazon[0], 15 + i*27, e.alto() - 40, 0, 0.01);
			}else {
				e.dibujarImagen(imagenesCorazon[1], 15 + i*27, e.alto() - 40, 0, 0.01);
			}
		}
	}
	
	public void dibujarMisilesRestantes(Entorno entorno, Proyectil[] misiles, int cantidadMisilesDisparados) {
		for(int i = 0; i < misiles.length; i++ ) {
			if(i < cantidadMisilesDisparados) {
				entorno.dibujarImagen(imagenMisilVacio, 20 , (250 + (10*i)) , 90);
			} else {
				entorno.dibujarImagen(imagenMisil, 20 , (250 + (10*i)) , 90);
			}
		}
	}
	
	public void restarCorazon() {
		if (cantCorazonesMenos < 3) {
			cantCorazonesMenos++;
			anchoBarraVida = 0;
		}
	}
	
	public void sumarCorazon() {
		this.cantCorazonesMenos--;
	}
	
//	GETTERS
	
	public int anchoBarraVida() {
		return anchoBarraVida;
	}
	
	public int getBarraVidaTotal() {
		return barraVidaTotal;
	}
	
	public int getCantCorazonesMenos() {
		return cantCorazonesMenos;
	}
}