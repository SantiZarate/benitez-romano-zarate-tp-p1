package escenarios;

import java.awt.Color;

import entorno.Entorno;

//ESTA CLASE SIRVE PARA PODER HACER LAS
//TRANSICION FADE IN Y FADE OUT.
//FUNCIONA GRACIAS A QUE SE PUEDE MODIFICAR
//EL APHA (OPACIDAD) DE LOS COLORES.

public class Transicion {
	private int ancho;
	private int largo;
	public Color color;
	public int opacidad;
	private boolean pantallazo;
	
	public Transicion(String nombre) {
		this.ancho = 800;
		this.largo = 600;
		this.opacidad = 255;
		this.pantallazo = false;
		
		if(nombre.equals("rojo")) {
			this.color = new Color(255, 0, 0, this.opacidad);
		}

		if(nombre.equals("verde")) {
			this.color = new Color(0, 255, 0, this.opacidad);
		}
	}
	
	public Transicion(int opacidad) {
		this.ancho = 800;
		this.largo = 600;
		this.opacidad = opacidad;
		this.color = new Color(0, 0, 0, this.opacidad);
	}
	
//	METODOS DE CLASE

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(entorno.ancho() / 2, entorno.alto() / 2, ancho, largo, 0, this.color);
	}

	public void subirOpacidad() {
		this.opacidad = this.opacidad + 5;
		this.color = new Color(0, 0, 0, this.opacidad);
	}

	public void bajarOpacidad() {
		this.opacidad = this.opacidad - 5;
		this.color = new Color(0, 0, 0, this.opacidad);
	}

	public void bajarOpacidadPorDaño() {
		this.opacidad = this.opacidad - 17;
		this.color = new Color(255, 0, 0, this.opacidad);
	}
	
	public void bajarOpacidadPasarNivel() {
		this.opacidad = this.opacidad - 17;
		this.color = new Color(0, 255, 0, this.opacidad);
	}

//	GETTERS

	public boolean esTransparente() {
		return this.opacidad == 0;
	}

	public boolean esOpaco() {
		return this.opacidad == 255;
	}

	public void fullOpaco() {
		this.opacidad = 255;
	}

	public void fullTransparente() {
		this.opacidad = 0;
	}

	public void reproducirPantallazo() {
		this.pantallazo = !this.pantallazo;
	}

	public boolean getPantallazo() {
		return pantallazo;
	}

//	DIBUJADO
	
	public void reproducirPantallazoVerde(Entorno entorno) {
		if (getPantallazo()) {
			if (!esTransparente()) {
				dibujar(entorno);
				bajarOpacidadPasarNivel();
			} else {
				fullOpaco();
				reproducirPantallazo();
			}
		}
	}
	
	public void reproducirPantallazoRojo(Entorno entorno) {
		if (getPantallazo()) {
			if (!esTransparente()) {
				dibujar(entorno);
				bajarOpacidadPorDaño();
			} else {
				fullOpaco();
				reproducirPantallazo();
			}
		}
	}
} // FIN DE CLASE
