package escenarios;

import java.awt.Image;
import java.util.LinkedList;

import entorno.Entorno;
import entorno.Herramientas;
import extras.Animacion;
import extras.Items;
import juego.Asteroide;
import juego.Enemigo;
import juego.Nave;
import juego.Proyectil;

public class Nivel {
	private Nave nave; //
	private Nave naveFantasma; //
	private Image fondo; //
	private double yFondo; //
	private Proyectil[] misil;
	private Enemigo[] enemigos;
	private Enemigo kamikaze;//
	private LinkedList<Double> CoordX; //
	private Proyectil[] ion; //
	private Asteroide[] asteroides; //
	private int contadorMisilesDisparados;
	
	// CONFIGS NIVEL
	private double puntajeParaPasarNivel;
	private double puntaje;
	private double distanciaMiPuntaje;
	private boolean pausa;
	private boolean juegoCompletado;
	private int numNivel;
	private int ticksRecarga;
	private int segundosJuego;
	private int contadorKills;
	private int mili;
	private Image imagenPausa;
	
	//EXTRAS -----------------------		
	
	private Animacion explosionNave;
	private Animacion explosionEnemiga;
	private Animacion explosionAsteroide;
	private Animacion animacionRecarga;
	private Animacion animacionGameOver;
	private Animacion animacionJuegoCompletado;
	private Animacion animacionNumeroNivel;
	private Transicion dañoPantallaMuerte;
	private Transicion transicion;
	private Transicion nivelCompletado;
	private Hud hud;
	private Items bonificadorPuntos;
	private Items bonificadorVida;
	private boolean bonificadorPuntosActivo;
	private int verificadorTimePuntos;

//	METODO CONSTRUCTOR
	public Nivel(Entorno entorno) {
		this.numNivel = 1;
		this.nave = new Nave(entorno.ancho() / 2, entorno.alto() - 55, "Nave.png");
		this.fondo = Herramientas.cargarImagen("fondo1.png");
		this.yFondo = 0;
		this.misil = new Proyectil[15];
		this.CoordX = new LinkedList<Double>();
		this.enemigos = new Enemigo[4];
		this.ion = new Proyectil[enemigos.length];

		// Crea coordenadas X (que no se superponen) para los enemigos
		CoordX.add(151 + Math.random() * (650 - 151));
		while (CoordX.size() != enemigos.length) {
			int verificador = 0;
			double num = (151 + Math.random() * (650 - 151));
			for (int j = 0; j < CoordX.size(); j++) {
				if (Math.abs(CoordX.get(j) - num) >= 70) {
					verificador++;
				}
				if (verificador == CoordX.size()) {
					CoordX.add(num);
					verificador = 0;
				}
			}
		}

		// Coloca las coordenadas X
		for (int j = 0; j < CoordX.size(); j++) {
			enemigos[j] = new Enemigo(CoordX.get(j), +-110, this.numNivel, "enemi.png");
		}
		
		asteroides = new Asteroide[4];

		for (int i = 0; i < asteroides.length; i++) {
			if (i % 2 == 0) {
				asteroides[i] = new Asteroide(Math.PI / 4, entorno, "asteroide_00.png");
			} else {
				asteroides[i] = new Asteroide((Math.PI / 4) * 3, entorno, "asteroide_01.png");
			}
		}

		this.hud = new Hud();
		this.explosionNave = new Animacion("res/explosion", 0.5);
		this.explosionEnemiga = new Animacion("res/explosion-enemigo", 0.25);
		this.explosionAsteroide = new Animacion("res/explosion-asteroide", 0.35);
		this.animacionRecarga = new Animacion("res/circulo-recarga", 1);
		this.animacionGameOver = new Animacion("res/game-over", 1);
		this.animacionJuegoCompletado = new Animacion("res/juego-completado", 1);
		this.animacionNumeroNivel = new Animacion();
		this.dañoPantallaMuerte = new Transicion("rojo");
		this.nivelCompletado = new Transicion("verde");
		this.transicion = new Transicion(255);
		
		this.contadorKills = 0;
		this.contadorMisilesDisparados = 0;
		this.ticksRecarga = 0;
		this.puntaje = 0.0;
		this.puntajeParaPasarNivel = 1000.0;
		this.verificadorTimePuntos = 0;
		this.imagenPausa = Herramientas.cargarImagen("pausa.png");
		
		this.pausa = false;
		this.juegoCompletado = false;
		this.bonificadorPuntosActivo = false;
	}

	// CREACION DE ELEMENTOS
	// ------------------------------------------------------------------------

	public Enemigo crearEnemigo(int j) {
		this.enemigos[j] = new Enemigo(CoordX.get(j), -50, this.numNivel, "enemi.png");
		return enemigos[j];
	}

	public Items crearBonificadorPuntos(int i) {
		if (i % 1000 == 0) {
			bonificadorPuntos = new Items(151 + Math.random() * (650 - 151), -50, "puntoss.png");
		}
		return bonificadorPuntos;
	}

	public Items crearBonificadorVida(int i) {
		if (i % 1000 == 0) {
			bonificadorVida = new Items(151 + Math.random() * (650 - 151), -50, "vida.png" );
		}
		return bonificadorVida;
	}

	// GETTERS
	// -----------------------------------------------------------------------------

	public boolean estaPausado() {
		return pausa;
	}

	public boolean juegoTerminado() {
		return hud.getCantCorazonesMenos() == 3;
	}

	public boolean juegoCompletado() {
		return juegoCompletado;
	}

//	-------------------------------------------------------------------------------------------------
	
	//COLISIONES  -----------------------------------------------------------------------------------

	public void colisionEnemigosConNave(Entorno entorno) {
		for (int i = 0; i < enemigos.length; i++) {
			if (enemigos[i] != null && nave != null
					&& colision(enemigos[i].getX(), enemigos[i].getY(), nave.getX(), nave.getY(), 30)) {
				explosionNave.empezarAnimacion(nave.getX(), nave.getY());
				enemigos[i] = null;
				naveFantasma = new Nave(nave.getX(), entorno.alto() - 55, "NaveMuerta.png");
				animacionRecarga.terminarAnimacion();
				nave = null;
				dañoPantallaMuerte.reproducirPantallazo();
				hud.restarCorazon();
			}
		}
	}

	public void colisionAsteroidesConNave(Entorno entorno) {
		for (int i = 0; i < asteroides.length; i++) {
			if (asteroides[i] != null && nave != null
					&& colision(asteroides[i].getX(), asteroides[i].getY(), nave.getX(), nave.getY(), 40)) {
				explosionNave.empezarAnimacion(nave.getX(), nave.getY());
				asteroides[i] = null;
				naveFantasma = new Nave(nave.getX() / 2, entorno.alto() - 55, "NaveMuerta.png");
				animacionRecarga.terminarAnimacion();
				nave = null;
				dañoPantallaMuerte.reproducirPantallazo();
				hud.restarCorazon();
			}
		}
	}

	public void colisionKamikazeConNave(Entorno entorno) {
		if (kamikaze != null && nave != null
				&& colision(kamikaze.getX(), kamikaze.getY(), nave.getX(), nave.getY(), 50)) {
			explosionNave.empezarAnimacion(nave.getX(), nave.getY());
			kamikaze = null;
			naveFantasma = new Nave(nave.getX(), entorno.alto() - 55, "NaveMuerta.png");
			animacionRecarga.terminarAnimacion();
			nave = null;
			dañoPantallaMuerte.reproducirPantallazo();
			hud.restarCorazon();
		}
	}

	public void colisionMisilConAsteroides() {
		for (int j = 0; j < asteroides.length; j++) {
			for (int p = 0; p < misil.length; p++) {
				if (misil[p] != null && asteroides[j] != null
						&& colision(misil[p].getX(), misil[p].getY(), asteroides[j].getX(), asteroides[j].getY(), 25)) {
					explosionAsteroide.empezarAnimacion(asteroides[j].getX(), asteroides[j].getY());
					asteroides[j] = null;
					misil[p] = null;
				}
			}
		}
	}

	public void colisionMisilConKamikaze() {
		for (int p = 0; p < misil.length; p++) {
			if (misil[p] != null && kamikaze != null
					&& colision(misil[p].getX(), misil[p].getY(), kamikaze.getX(), kamikaze.getY(), 25)) {
				explosionAsteroide.empezarAnimacion(kamikaze.getX(), kamikaze.getY());
				kamikaze = null;
				misil[p] = null;
			}
		}
	}
	
	public void colisionIonConNave() {
		for (int j = 0; j < ion.length; j++) {
			if (ion[j] != null) {
				if (ion[j] != null && nave != null && colision(ion[j].getX(), ion[j].getY(), nave.getX(), nave.getY(), 30)) {
					ion[j] = null;
					hud.bajarBarraVida(20 + (5 * numNivel));
					dañoPantallaMuerte.reproducirPantallazo();
				}
			}
		}
	}

	public void colisionMisilConEnemigo() {
		for (int j = 0; j < enemigos.length; j++) {
			for (int p = 0; p < misil.length; p++) {
				if (misil[p] != null && enemigos[j] != null
						&& colision(misil[p].getX(), misil[p].getY(), enemigos[j].getX(), enemigos[j].getY(), 25)) {
					explosionEnemiga.empezarAnimacion(enemigos[j].getX(), enemigos[j].getY());
					enemigos[j] = null;
					misil[p] = null;
					contadorKills++;
					puntaje += 100;
					
					if (bonificadorPuntosActivo)
							puntaje += 100;
						
					int guarda = j;
					while (CoordX.get(guarda) != CoordX.get(j)) {
						int verificadorX = 0;
						double num = (151 + Math.random() * (650 - 151));
						for (int o = 0; o < CoordX.size(); o++) {
							if (Math.abs(CoordX.get(o) - num) >= 80) {
								verificadorX++;
							}
							if (verificadorX == CoordX.size()) {
								CoordX.set(j, num);
								verificadorX = 0;
							}
						}
					}
				}
			}
		}
	}

	public void colisionBonificadoresConNave() {
		if(bonificadorPuntos != null) {
			if (nave != null && colision(bonificadorPuntos.getX(), bonificadorPuntos.getY(), nave.getX(), nave.getY(), 35)) {
				bonificadorPuntosActivo = true;
				bonificadorPuntos = null;
			}
			
			if (bonificadorPuntosActivo) {
				verificadorTimePuntos++;
				if (verificadorTimePuntos == 300) {
					verificadorTimePuntos = 0;
					bonificadorPuntosActivo = false;
				}
			}
		}
		
		if(bonificadorVida != null) {
			if (nave != null & bonificadorVida != null
					&& colision(bonificadorVida.getX(), bonificadorVida.getY(), nave.getX(), nave.getY(), 35)) {
				bonificadorVida = null;
				
				if (hud.getCantCorazonesMenos() != 0)
					hud.sumarCorazon();
			}
		}
	}

//	-------------------------------------------------------------------------------------------------

//	DIBUJADOS / COMPORTAMIENTOS  --------------------------------------------------------------------

	public void dibujarNave(Entorno entorno, int contadorTicks) {
		if (nave != null) {
			// MOVIEMIENTO NAVE
			if (entorno.estaPresionada('a') && nave.getX() > 0) {
				nave.moverIzq();
			}

			if (entorno.estaPresionada('d') && nave.getX() < entorno.ancho()) {
				nave.moverDer();
			}

			// DIBUJADO DE LA NAVE
			nave.dibujar(entorno);

			// DISPARO DE MISILES
			if ((entorno.sePresiono(entorno.TECLA_ESPACIO) || entorno.sePresiono(entorno.TECLA_ARRIBA))
					&& contadorMisilesDisparados < misil.length && !nave.getRecarga()) {
				misil[contadorMisilesDisparados] = new Proyectil(nave.getX(), nave.getY(),"misil_nave.png");
				contadorMisilesDisparados++;
			}
			
			// RECARGA DE MISILES
			if (entorno.sePresiono('r') && contadorMisilesDisparados != 0) {
				nave.recargar();
			}

			// UNA VEZ LOS MISILES LLEGA A 15 SE REINICIA EL CONTADOR
			// SE ESTA FORMA LOS MISILES SON INFINITOS
			if (contadorMisilesDisparados == misil.length) {
				ticksParaRecargar();
				animacionRecarga.empezarAnimacion(nave.getX() + 25, nave.getY() - 20);
				if (ticksParaRecargar() % 180 == 0) {
					contadorMisilesDisparados = 0;
					ticksRecarga = 0;
				}
			}

			// Animacion de recarga
			if (nave.getRecarga()) {
				ticksParaRecargar();
				animacionRecarga.empezarAnimacion(nave.getX() + 25, nave.getY() - 20);
				if (ticksParaRecargar() % 180 == 0) {
					contadorMisilesDisparados = 0;
					ticksRecarga = 0;
					nave.descargar();
				}
			}
		} else {
			animacionRecarga.terminarAnimacion();

			if (entorno.estaPresionada('a') && naveFantasma.getX() > 0) {
				naveFantasma.moverIzq();
			}

			if (entorno.estaPresionada('d') && naveFantasma.getX() < entorno.ancho()) {
				naveFantasma.moverDer();
			}

			// NAVE FANTASMA TITILANTE
			if (contadorTicks % 10 != 0) {
				naveFantasma.dibujar(entorno);
			}
		}

		// REAPARICION DE LA NAVE AL PRESIONAR ENTER Y DESTRUCCION NAVE FANTASMA
		if (nave == null && entorno.sePresiono(entorno.TECLA_ENTER)) {
			nave = new Nave(naveFantasma.getX(), entorno.alto() - 55, "Nave.png");
			contadorMisilesDisparados = 0;
			naveFantasma = null;
			hud.restablecerBarraVida();
		}
	}

	public void dibujarMisiles(Entorno entorno) {
		for (int p = 0; p < misil.length; p++) {
			if (misil[p] != null) {
				misil[p].dibujar(entorno);
				misil[p].avanzar(-4);

				// UNA VEZ EL MISIL SALE DE LA PANTALLA SE DESTRUYE
				if (misil[p].getY() < -10) {
					misil[p] = null;
				}
			}
		}
	}

	public void dibujarIones(Entorno entorno) {
		for (int j = 0; j < ion.length; j++) {
			if (ion[j] != null) {

				ion[j].dibujar(entorno);
				ion[j].avanzar(5 + numNivel);

				if (ion[j].getY() > entorno.alto() + 50) {
					ion[j] = null;
				}
			}
		}
	}

	public void dibujarAsteroides(Entorno entorno) {
		for (int a = 0; a < asteroides.length; a++) {
			if (asteroides[a] != null && segundosJuego > 3) {
				asteroides[a].dibujarAsteroide(entorno);
				asteroides[a].avanzar();
				asteroides[a].girar();

				if (asteroides[a].estaFuera(entorno)) {
					asteroides[a].cambiarAngulo();
				}

				if (asteroides[a].y > entorno.alto() + 50) {
					asteroides[a] = null;
				}
			} else {
				if (a % 2 == 0) {
					asteroides[a] = new Asteroide(Math.PI / 4, entorno, "asteroide_00.png");
				} else {
					asteroides[a] = new Asteroide((Math.PI / 4) * 3, entorno, "asteroide_01.png");
				}
			}
		}
	}

	public void dibujarHud(Entorno entorno, int contadorTicks) {
		hud.dibujar(entorno, contadorTicks, puntaje, contadorKills, puntajeParaPasarNivel, distanciaMiPuntaje, numNivel);
		
		// SI LA BARRA DE VIDA LLEGA A 0 SE DESTRUYE LA NAVE
		if (hud.anchoBarraVida() <= 0 && nave != null) {
			explosionNave.empezarAnimacion(nave.getX(), nave.getY());
			naveFantasma = new Nave(entorno.ancho() / 2, entorno.alto() - 55, "NaveMuerta.png");
			hud.restarCorazon();
			nave = null;
		}

		// DIBUJADO DE MISILES A LA IZQUIERDA DE LA PANTALLA
		hud.dibujarMisilesRestantes(entorno, misil, contadorMisilesDisparados);
	}

	public void dibujarFondo(Entorno entorno) {
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, (entorno.alto() / 2) + yFondo, 0, 1);
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, (-entorno.alto() / 2) + yFondo, 0, 1);
		yFondo++;

		if (yFondo > 600) {
			yFondo = 0;
		}
	}

	public void dibujarKamikaze(Entorno entorno) {
		if (segundosJuego % 10 == 0 && segundosJuego > 1) {
			kamikaze = new Enemigo(200, -50, 5 + numNivel, "kamikaze.png");
		}

		if (kamikaze != null) {
			kamikaze.dibujar(entorno);
			kamikaze.avanzarK();
			if (nave != null) {
				kamikaze.perseguir(nave.getX(), nave.getY(), entorno);
			}

			if (kamikaze.getY() > entorno.alto() + 30) {
				kamikaze = null;
			}
		}
	}

	public void dibujarEnemigos(Entorno entorno, int contadorTicks) {

		for (int j = 0; j < enemigos.length; j++) {
			// Dibuja a los enemigos
			if (enemigos[j] != null && CoordX.get(j) != null && segundosJuego > 3) {

				enemigos[j].dibujar(entorno);
				enemigos[j].avanzar(mili);
				// Se crean los proyectiles enemigos
				if (ion[j] == null && enemigos[j].getY() > entorno.alto() * 0.1
						&& enemigos[j].getY() < entorno.alto() * 0.6) {
					ion[j] = new Proyectil(enemigos[j].getX(), enemigos[j].getY(), "misil_enemigo.png");
				}
				// Verifica si enemigo esta fuera de pantalla
				if (!enemigos[j].estaFuera(entorno)) {
					enemigos[j] = null;

					int guarda = j;
					while (CoordX.get(guarda) != CoordX.get(j)) {
						int verificadorX = 0;
						double num = (151 + Math.random() * (650 - 151));
						for (int p = 0; p < CoordX.size(); p++) {
							if (Math.abs(CoordX.get(p) - num) >= 80) {
								verificadorX++;
							}
							if (verificadorX == CoordX.size()) {
								CoordX.set(j, num);
								verificadorX = 0;
							}
						}
					}
				}

			} else {
				if (contadorTicks % 120 == 0) {
					crearEnemigo(j);
				}
			}
		}
	}

	public void dibujarbonificadores(Entorno entorno, int contadorTicks) {
		if (crearBonificadorPuntos(contadorTicks) != null) {
			bonificadorPuntos.dibujar(entorno);
			bonificadorPuntos.avanzar();
		} else {
			crearBonificadorPuntos(contadorTicks);
		}
		
		if (crearBonificadorVida(contadorTicks) != null) {
			bonificadorVida.dibujar(entorno);
			bonificadorVida.avanzar();
		} else {
			crearBonificadorVida(contadorTicks);
		}
	}

//	---------------------------------------------------------------------------------------------------

//	ANIMACIONES ---------------------------------------------------------------------------------------

	public void animExplosiones(Entorno entorno, int contadorTicks) {
		explosionEnemiga.reproducirExplosion(contadorTicks, entorno, 2);

		explosionAsteroide.reproducirExplosion(contadorTicks, entorno, 2);

		explosionNave.reproducirExplosion(contadorTicks, entorno, 0);
	}

	public void animRecargar(Entorno entorno) {
		animacionRecarga.reproducirRecarga(entorno, ticksRecarga);
	}

	public void animPantallazos(Entorno entorno) {
		dañoPantallaMuerte.reproducirPantallazoRojo(entorno);
		nivelCompletado.reproducirPantallazoVerde(entorno);
	}

	public void animJuegoCompletado(Entorno entorno, int contadorTicks) {
		animacionJuegoCompletado.reproducirJuegoCompletado(entorno, contadorTicks);
	}

	public void animGameOver(Entorno entorno, int contadorTicks) {
		animacionGameOver.reproducirGameOver(entorno, contadorTicks);
	}

	public void animNuevoNivel(Entorno entorno, int contadorTicks) {
		if (segundosJuego < 3 && numNivel < 6) {
			animacionNumeroNivel.reproducirNuevoNivel(entorno, contadorTicks);
		}
	}
	
//	---------------------------------------------------------------------------------------------------
	
//	AUXILIARES ----------------------------------------------------------------------------------------

	public void aumentarSegundosJuego(int i) {
		if (i % 60 == 0) {
			segundosJuego++;
		}
	}

	public int ticksParaRecargar() {
		ticksRecarga++;
		return ticksRecarga;
	}

	public void controlPuntaje() {
		if (puntaje >= puntajeParaPasarNivel) {
			nivelCompletado.reproducirPantallazo();
			segundosJuego = 0;
			puntaje = 0;
			puntajeParaPasarNivel += 500 * numNivel;
			this.asteroides = new Asteroide[4 + this.numNivel];
			numNivel++;
			
			if (numNivel < 6) {
				animacionNumeroNivel.aumentarNivel();
			}
			
			// SE VERIFICA SI SE TERMINO EL JUEGO
			if (numNivel == 6) {
				animacionJuegoCompletado.empezarAnimacion(400, 300);
				juegoCompletado = true;
			}
		}
	}

	public void aumentarCronometro(Entorno entorno) {
		if (mili++ == 60) {
			mili = 0;
		}
	}

	public static boolean colision(double x1, double y1, double x2, double y2, double d) {
		return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) < d * d;
	}

	public void entrada(Entorno entorno) {
		transicion.dibujar(entorno);

		if (!transicion.esTransparente()) {
			transicion.bajarOpacidad();
		}
	}

	public void dibujadoJuegoPausado(Entorno entorno) {
		entorno.dibujarImagen(imagenPausa, entorno.ancho()/2, entorno.alto()/2, 0 );
	}

	public void pausar() {
		this.pausa = !this.pausa;
	}
}