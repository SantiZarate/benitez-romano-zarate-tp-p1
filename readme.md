# Proyecto Java :tw-1f680:

Prototipo de videojuego espacial llevado a cabo con Java, por Tomas Benitez, Alexis Romano y Santiago Zarate.

> El objetivo de este trabajo pr´actico es desarrollar un video juego en el cual la Astro-MegaShip
elimine la mayor cantidad de Destructores Estelares, sin ser destruida en el intento.

------------


####Todo List:

- En El Centro Inferior De La Pantalla Deberá Verse Astro-Megaship Y Algunos Obstáculos Representados Por Asteroides. La Cantidad de asteroides debe ser entre 4 y 6.

- algunos asteroides deben moverse en diagonal hacia la derecha y otros en diagonal hacia la izquierda.

- ~~Permitir a la nave morse horizontalmente, sin salirse de los bordes.~~

- ~~Implementar la posibilidad de disparar de parte de la nave, la bala se elimina al colisionar con un enemigo, asteroide o alcanzar el borde superior de la pantalla.~~

- Los enemigos deben descender alternando su movimienot horizontal de izquierda a derecha. no se pueden superporner a otras naves enemigas ni asteroides.

- Los enemigos pueden disparar tambien, estas balas no se destruyen al superponerse a los asteroides.

- ~~La nave se muere una vez es impactada por una bala enemiga o por colisionar con un asteroide.~~

- ~~Una vez eliminada una nave enemiga, esta vuelve a renacer despues de cierto cierto tiempo.~~ *Parcialmente resuelto, se puede mejorar*

- Una vez no haya mas enemigos, se da por finalizado el nivel con un cartel en la pantalla.

- ~~Mostrar cantidad de enemigos eliminados en algun lugar de la pantalla.~~

- Hacer el codigo lo mas prolijo posible.

**OPCIONALES**

- Destructores Estelares especiales que sean kamikazes: cuando detectan la presencia de La Astro-MegaShip vuelan r´apidamente hacia su ubicaci´on para que ambas exploten.
- Varias vidas, o niveles de energ´ıa que vayan disminuyendo a medida que los Destructores Estelares hagan contacto con la Astro-MegaShip.

- Que aparezcan items en distintos lugares del juego que vayan cayendo que le den puntaje/vidas extra a la Astro-MegaShip.

- Niveles: La posibilidad de comenzar un nuevo nivel despu´es de jugar determinada cantidad de tiempo o despu´es de determinada cantidad de puntaje acumulado, e incrementar la dificultad y/o velocidad de cada nivel.

- Que aparezca un Destructor Estelar colosal como jefe final o de nivel.
